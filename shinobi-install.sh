#!/bin/bash

# Check OS and set default directory
OSTYPE="$(uname -s)"

if [ "$OSTYPE" = "Darwin" ]; then
    defaultDirectory="/Applications"
else 
	defaultDirectory="/home"
fi

echo "========================================================="
echo "==   Shinobi : The Open Source CCTV and NVR Solution   =="
echo "========================================================="
echo "This script will install Shinobi CCTV with minimal user"
echo "intervention. When prompted you may answer yes by typing"
echo "the letter [Y] and pressing [Enter]."
echo 
echo "========================================================="
read -p "Press [Enter] to begin..."

#Installation directory selection
echo "========================================================="
echo "Step 1: Select an installation location for Shinobi"
echo "The default installation location is $defaultDirectory"
echo "Select a different installation location?"
read -p "[Y]es or [N]o?" customLocation

#Changes input to uppercase
customLocation=${customLocation^}

if [ "$customLocation" = "Y" ]; then
    until [ "$installLocationComplete" = "Y" ]; do
		read -p "Please enter the directory you wish to install Shinobi in. " -r installLocation
		read -p "Install Shinobi in $installLocation? " installLocationConfirmation
		#Changes input to uppercase
		installLocationConfirmation=${installLocationConfirmation^}
		if [ "$installLocationConfirmation" = "Y" ]; then
			if [ -d "$installLocation" ]; then
				echo "Directory $installLocation already exists, continuing with installation"
				installLocationComplete="Y"
			else 
				read -p "$installLocation does not exist. Do you want to create this directory now?" createDirectoryConfirmation
				#Changes input to uppercase
				createDirectoryConfirmation=${createDirectoryConfirmation^}
				if [ "$createDirectoryConfirmation" = "Y" ]; then
					mkdir -p "$installLocation"
					echo "Directory $installLocation created"
					installLocationComplete="Y"
				else
					echo "Directory $installLocation does not exist and will not be created."
					read -p "Do you want to use the default installation directory: $defaultDirectory?" useDefault
					#Changes input to uppercase
					useDefault=${useDefault^}
					if [ "$useDefault" = "Y" ]; then
						echo "Installing Shinobi in $defaultDirectory"
						installLocation="$defaultDirectory"
						installLocationComplete="Y"
					else
						echo "Directory $installLocation does not exist and will not be created."
						echo "User delcined default installation directory. Exiting..."
						exit 1
					fi
				fi
			fi
		fi
	done
else
	installLocation="$defaultDirectory"
fi

echo "========================================================="
echo "Changing directory to: \"$installLocation\""
cd $installLocation || { echo "Failed to change to $installLocation"; exit 1; }
echo "========================================================="

#Check if Shinobi directory exists
if [ ! -d "Shinobi" ]; then
    #Check if Mac OS and if Git is needed
    if [ "$OSTYPE" = "Darwin" ]; then
        if [ ! -x "$(command -v brew)" ]; then
            ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
            brew doctor
        fi
        if [ ! -x "$(command -v git)" ]; then
            brew install git
        fi
    else
        #Check if user is root
        if [ "$(id -u)" != 0 ]; then
            echo "========================================================="
            echo "Shinobi requires being run as root."
            echo "Do you want to continue without being root?"
            read -p "[Y]es or [n]o? Default: Yes" nonRootUser
			
			#Changes input to uppercase
			nonRootUser=${nonRootUser^}
            if [  "$nonRootUser" = "N" ]; then
                echo "Cancelling installation..."
                exit 1
            fi
        fi
        # Check if Git is needed
        if [ ! -x "$(command -v git)" ]; then
            # Check if Ubuntu
            if [ -x "$(command -v apt)" ]; then
                sudo apt update
                sudo apt install git -y
            fi
            # Check if Cent OS
            if [ -x "$(command -v yum)" ]; then
                sudo yum update
                sudo yum install git -y
				CentOSInstaller=1
            fi
        fi
        # Check if wget is needed
        if [ ! -x "$(command -v wget)" ]; then
            # Check if Ubuntu
            if [ -x "$(command -v apt)" ]; then
                sudo apt install wget -y
            fi
            # Check if Cent OS
            if [ -x "$(command -v yum)" ]; then
                sudo yum install wget -y
            fi
        fi
    fi
		
    echo "========================================================="
	echo "==          Install Shinobi CE or Shinobi Pro          =="
    echo "========================================================="
    echo "==        Shinobi Pro is free for personal use.        =="
    echo "==       Learn more at http://shinobi.video/pro        =="
    echo "========================================================="
    read -p "[C]E or [P]ro? Default: Pro " repoChoice
	echo "========================================================="
	
	#Changes input to uppercase
	repoChoice=${repoChoice^}
	
    if [ "$repoChoice" = "C" ] || [ "$repoChoice" = "CE" ]; then
        productName="Shinobi Community Editon (CE)"
        theRepo='CE'
        theBranch='master'
    else
        theRepo=''
        productName="Shinobi Professional Edition (Pro)"
		echo "========================================================="
        echo "Install the Development branch?"
        read -p "[Y]es or [n]o? Default: Yes " branchChoice
        
		#Changes input to uppercase
		branchChoice=${branchChoice^}
		
		echo "========================================================="
		
        if [ "$branchChoice" = "N" ]; then
            echo "Getting the Master Branch"
            theBranch='master'
        else
			echo "Getting the Development Branch"
            theBranch='dev'
        fi
    fi
    # Download from Git repository
    gitURL="https://gitlab.com/Shinobi-Systems/Shinobi$theRepo"
    sudo git clone $gitURL.git -b $theBranch Shinobi
    # Enter Shinobi folder 
    cd Shinobi || { echo "Failed to change to Shinobi directory"; exit 1; }
    gitVersionNumber=$(git rev-parse HEAD)
    theDateRightNow=$(date)
    # write the version.json file for the main app to use
    sudo touch version.json
    sudo chmod 777 version.json
    sudo echo '{"Product" : "'"$productName"'" , "Branch" : "'"$theBranch"'" , "Version" : "'"$gitVersionNumber"'" , "Date" : "'"$theDateRightNow"'" , "Repository" : "'"$gitURL"'"}' > version.json
    echo "========================================================="
    echo "==   Shinobi : The Open Source CCTV and NVR Solution   =="
	echo "========================================================="
    echo "Repository : $gitURL"
    echo "Product : $productName"
    echo "Branch : $theBranch"
    echo "Version : $gitVersionNumber"
    echo "Date : $theDateRightNow"
    echo "========================================================="
else
    echo "========================================================="
    echo "Shinobi already downloaded."
    cd Shinobi || { echo "Failed to change to Shinobi directory"; exit 1; }
fi
# start the installer in the main app (or start shinobi if already installed)
echo "========================================================="
sudo chmod +x INSTALL/start.sh
sudo INSTALL/start.sh